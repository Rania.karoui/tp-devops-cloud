# Application d'exemple pour le travail pratique sur le Cloud

## Installation

```bash
git clone https://gitlab.com/BDWA-SAS-2/cloud/example-app
cd example-app
npm install # Installation des dépendances
```

## Lancement

```bash
npm start
```


Token session: 09bd1e24687140508bb6142e4a3ee9634d0a10e5 
